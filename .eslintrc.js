module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint/eslint-plugin',
  ],
  extends: [
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    // "plugin:ramda/recommended",
    "plugin:security/recommended",
    'prettier',
    'prettier/@typescript-eslint',
    "airbnb-base",
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  settings: {
    "import/resolver": {
      "node": {
        "extensions": [".js", ".jsx", ".ts", ".tsx"]
      }
    }
  },
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    'security/detect-non-literal-fs-filename': 0,
    "import/extensions": "off",
    "class-methods-use-this": 0,
    "padding-line-between-statements": [
      "error",
      { "blankLine": "always", "prev": "*", "next": "return" },
      { "blankLine": "always", "prev": "block-like", "next": "*" },
      { "blankLine": "always", "prev": "*", "next": "block-like" },
      { "blankLine": "always", "prev": "*", "next": "block-like" },
      { "blankLine": "always", "prev": ["const", "let", "var"], "next": "*" },
      { "blankLine": "any", "prev": ["const", "let", "var"], "next": ["const", "let", "var"] },
      { "blankLine": "always", "prev": "function", "next": "function" }
    ],
    "no-return-await": 0,
    "no-useless-constructor": 0,
    "no-empty-function": 0,
    "no-console": "error",
    "no-multiple-empty-lines": ["error", { "max": 1 }],
    "prefer-destructuring": ["error", { "object": true, "array": false }],
    "no-underscore-dangle": 0,
    "no-unused-vars": "error",
    "lines-between-class-members": 0,
    "implicit-arrow-linebreak": 0,
    "arrow-parens": 0,
    "no-debugger": 1,
    "comma-dangle": ["error", "always-multiline"],
    "max-len": [
      "error",
      120,
      2,
      {
        "ignoreUrls": true,
        "ignoreComments": false,
        "ignoreRegExpLiterals": true,
        "ignoreStrings": true,
        "ignoreTemplateLiterals": true
      }
    ],
    "no-mixed-operators": 0,
    "no-confusing-arrow": 0,
    "no-param-reassign": [2],
    "import/no-extraneous-dependencies": 0,
    "no-array-constructor": 0,
    "import/prefer-default-export": 0,
    "linebreak-style": 0,
  },
};
