import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  MessageBody,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket, Server } from 'socket.io';

import { IMessageToServer } from './interfaces/message-to-server.interface';
import { ChatService } from './chat.service';

@WebSocketGateway({ namespace: 'chat' })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;
  private logger: Logger = new Logger('ChatGateway');

  constructor(
    private readonly chatService: ChatService,
  ) { }

  afterInit() {
    this.logger.log('Ws server was init');
  }

  handleConnection(client: Socket) {
    this.logger.log(`Client was connected ${client.id}`);
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client was disconnected ${client.id}`);
  }

  @SubscribeMessage('joinRoom')
  handleJoinRoom(client: Socket, room: string) {
    this.logger.log(`Connected room: ${room}`);
    client.join(room);
    client.emit('joinedRoom', room);
  }

  @SubscribeMessage('leaveRoom')
  handleLeaveRoom(client: Socket, room: string) {
    client.leave(room);
    client.emit('leftRoom', room);
  }

  @SubscribeMessage('chatToServer')
  async handleEvent(
    @MessageBody() {
      roomId,
      roomHash,
      sender,
      message,
      date,
    }: IMessageToServer,
  ): Promise<void> {
    await this.chatService.setMessage(roomHash, JSON.stringify({ sender, message, date }));

    this.server.to(roomId).emit('chatToClient', {
      sender,
      message,
      date,
    });
  }
}
