export interface IMessageToServer {
  readonly sender: string;
  readonly roomId: string;
  readonly roomHash: string;
  readonly message: string;
  readonly date: Date;
}
