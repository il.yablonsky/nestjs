export interface IMessageToClient {
  readonly sender: string;
  readonly message: string;
  readonly date: Date;
}
