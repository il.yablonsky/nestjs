import { Injectable, BadGatewayException } from '@nestjs/common';
import * as IORedis from 'ioredis';
import * as R from 'ramda';

import { IMessageToClient } from './interfaces/message-to-client.interface';

@Injectable()
export class ChatService {
  private client: IORedis.Redis;

  constructor() {
    this.client = new IORedis(process.env.REDISCLOUD_URL);
  }

  async setMessage(key: string, message: string) {
    try {
      await this.client.lpush(key, message);
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async getMessages(key: string) {
    try {
      const messages = await this.client.lrange(key, 0, -1);

      return R.compose<
        string[],
        string[],
        IMessageToClient[]
      >(
        R.map(JSON.parse),
        R.reverse,
      )(messages);
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async clearMessages(key: string) {
    try {
      await this.client.del(key);

      return true;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }
}
