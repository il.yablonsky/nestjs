import {
  Controller,
  Get,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ChatService } from './chat.service';
import { CreateRoomHashDto } from './dto/crate-room-hash.dto';

@Controller('chat')
export class ChatController {
  constructor(
    private readonly chatService: ChatService,
  ) { }

  @UseGuards(AuthGuard('jwt'))
  @Get('/get-chat-messages')
  async getMessages(@Query() { roomHash }: CreateRoomHashDto) {
    return await this.chatService.getMessages(roomHash);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/clear-chat-messages')
  async clearMessages(@Query() { roomHash }: CreateRoomHashDto) {
    return await this.chatService.clearMessages(roomHash);
  }
}
