/* eslint-disable @typescript-eslint/camelcase */
import { Injectable, BadGatewayException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as cloudinary from 'cloudinary';

import { CreateAvatarDto } from '../user/dto/create-avatar.dto';
import { IAvatar } from './interfaces/avatar.interface';
import { isEmptyOrNil } from '../helpers';

@Injectable()
export class AvatarService {
  constructor(
    @InjectModel('Avatar') private readonly AvatarModel: Model<IAvatar>,
  ) {
    cloudinary.v2.config({
      cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
      api_key: process.env.CLOUDINARY_API_KEY,
      api_secret: process.env.CLOUDINARY_API_SECRET,
    });
  }

  async getAvatar(uId: string) {
    return await this.AvatarModel.findOne({ uId }).exec();
  }

  async getAllAvatars() {
    return await this.AvatarModel.find().exec();
  }

  async setAvatar({
    uId,
    avatar,
    avatarName,
  }: CreateAvatarDto) {
    try {
      const avatarItem = await this.getAvatar(uId);

      if (!isEmptyOrNil(avatarItem)) {
        await cloudinary.v2.uploader.destroy(avatarItem.avatarId);
      }

      const result = await cloudinary.v2.uploader.upload(avatar);

      if (await this.AvatarModel.exists({ uId })) {
        await this.AvatarModel.updateOne({ uId }, { avatar: result.url, avatarName });

        return { avatar: result.url };
      }

      const createdAvatar = new this.AvatarModel({
        uId,
        avatar: result.url,
        avatarName,
        avatarId: result.public_id,
      });

      await createdAvatar.save();

      return { avatar: result.url };
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }
}
