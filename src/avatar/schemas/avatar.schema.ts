import { Schema, Types } from 'mongoose';

export const AvatarSchema = new Schema({
  uId: { type: Types.ObjectId, required: true, ref: 'User' },
  avatar: { type: String, required: true },
  avatarName: { type: String, required: true },
  avatarId: { type: String, required: true },
  date: { type: Date, default: Date.now },
});
