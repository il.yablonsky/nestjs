import { Document } from 'mongoose';

export interface IAvatar extends Document {
  uId: string;
  avatar: string;
  avatarName: string;
  avatarId: string;
  date: string;
}
