import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AvatarService } from './avatar.service';
import { AvatarSchema } from './schemas/avatar.schema';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Avatar', schema: AvatarSchema },
  ])],
  providers: [AvatarService],
  exports: [AvatarService],
})
export class AvatarModule {}
