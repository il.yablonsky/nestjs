import {
  Controller,
  Get,
  Post,
  Body,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { UserService } from './user.service';
import { CreateAvatarDto } from './dto/create-avatar.dto';

@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @UseGuards(AuthGuard('jwt'))
  @Get('/get-all-users')
  async getAllUsers() {
    return await this.userService.findAll();
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/set-avatar')
  async setUserAvatar(@Body() createAvatarDto: CreateAvatarDto) {
    return await this.userService.setAvatar(createAvatarDto);
  }
}
