import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import * as R from 'ramda';

import { AvatarService } from '../avatar/avatar.service';
import { CreateUserDto } from './dto/create-user.dto';
import { CreateAvatarDto } from './dto/create-avatar.dto';
import { IUser } from './interfaces/user.interface';
import { hashPassword } from '../helpers';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') private readonly UserModel: Model<IUser>,
    private readonly avatarService: AvatarService,
  ) { }

  async create(createUserDto: CreateUserDto) {
    const hash = await hashPassword(createUserDto.password);
    const createdUser = new this.UserModel(R.mergeDeepRight(createUserDto, { password: hash }));

    return await createdUser.save();
  }

  async find(id: string) {
    return await this.UserModel.findById(id).exec();
  }

  async findByEmail(email: string) {
    return await this.UserModel.findOne({ email }).exec();
  }

  async findAll() {
    const avatars = await this.avatarService.getAllAvatars();
    const users = await this.UserModel.find({}).exec();

    return R.map((item) => {
      const avatarItem: any = R.find(R.propEq('uId', item._id))(avatars);

      return {
        ...R.pick(['_id', 'firstname', 'lastname'], item),
        avatar: avatarItem?.avatar || null,
      };
    }, users);
  }

  async update(_id: Types.ObjectId, payload: Record<string, string>) {
    return await this.UserModel.updateOne({ _id }, payload);
  }

  async setAvatar(createAvatarDto: CreateAvatarDto) {
    return await this.avatarService.setAvatar(createAvatarDto);
  }
}
