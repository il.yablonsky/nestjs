import { Schema } from 'mongoose';

import { statusEnum } from '../enums/status.enum';

export const UserSchema = new Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  status: { type: String, enum: Object.values(statusEnum), default: statusEnum.pending },
  date: { type: Date, default: Date.now },
});

