export interface IReadableUser {
  readonly _id: string;
  readonly firstname: string;
  readonly lastname: string;
  readonly email?: string;
  readonly accessToken?: string;
  readonly avatar?: string;
}
