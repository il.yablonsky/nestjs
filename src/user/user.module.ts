import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AvatarModule } from '../avatar/avatar.module';
import { UserService } from './user.service';
import { UserSchema } from './schemas/user.schema';
import { UserController } from './user.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'User', schema: UserSchema },
    ]),
    AvatarModule,
  ],
  providers: [UserService],
  exports: [UserService],
  controllers: [UserController],
})
export class UserModule { }
