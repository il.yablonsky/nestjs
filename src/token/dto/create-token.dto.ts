import { IsString, IsDateString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';

export class CreateTokenDto {
  @ApiProperty()
  @IsString()
  token: string;

  @ApiProperty()
  @IsString()
  uId: mongoose.Types.ObjectId;

  @ApiProperty()
  @IsDateString()
  expireAt: string;
}
