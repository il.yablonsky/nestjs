import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { TokenService } from './token.service';
import { TokenSchema } from './schemas/token.schema';
import { JwtStrategy } from '../auth/strategies/jwt.strategy';
import { AvatarModule } from '../avatar/avatar.module';
import { UserModule } from '../user/user.module';
import { configModule } from '../config.root';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1d' },
    }),
    MongooseModule.forFeature([
      { name: 'Token', schema: TokenSchema },
    ]),
    UserModule,
    AvatarModule,
    configModule,
  ],
  providers: [JwtStrategy, TokenService],
  exports: [TokenService],
})
export class TokenModule {}
