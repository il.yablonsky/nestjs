import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { Model, Types } from 'mongoose';
import { SignOptions } from 'jsonwebtoken';
import * as moment from 'moment';

import { IUser } from '../user/interfaces/user.interface';
import { CreateTokenDto } from './dto/create-token.dto';
import { UserService } from '../user/user.service';
import { AvatarService } from '../avatar/avatar.service';
import { IToken } from './interfaces/token.interface';

@Injectable()
export class TokenService {
  constructor(
    @InjectModel('Token') private readonly TokenModel: Model<IToken>,
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
    private readonly avatarService: AvatarService,
  ) {}

  async create(createTokenDto: CreateTokenDto) {
    const token = new this.TokenModel(createTokenDto);

    return await token.save();
  }

  async findByToken(token: string) {
    return await this.TokenModel.findOne({ token });
  }

  async delete(uId: Types.ObjectId, token: string) {
    return await this.TokenModel.deleteOne({ uId, token });
  }

  async deleteAll(uId: Types.ObjectId) {
    return await this.TokenModel.deleteMany({ uId });
  }

  async exists(uId: Types.ObjectId, token: string) {
    return await this.TokenModel.exists({ uId, token });
  }

  async checkIsTokenExpired(token: string) {
    try {
      const data = await this.verifyToken(token);
      const user = await this.userService.find(data._id);
      const avatarData = await this.avatarService.getAvatar(data._id);

      return {
        _id: user._id,
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        avatar: avatarData?.avatar ?? null,
      };
    } catch (error) {
      throw new UnauthorizedException();
    }
  }

  async generateToken(data: any, options?: SignOptions) {
    return this.jwtService.sign(data, options);
  }

  async verifyToken(token: string) {
    try {
      const data = this.jwtService.verify(token);
      const isTokenExists = await this.exists(data._id, token);

      if (isTokenExists) {
        return data;
      }

      throw new UnauthorizedException();
    } catch (error) {
      throw new UnauthorizedException();
    }
  }

  async getAccessToken(user: IUser) {
    const token = await this.generateToken({
      _id: user._id,
      status: user.status,
    });
    const expireAt = moment().add(7, 'd').toISOString();

    await this.create({
      token,
      expireAt,
      uId: user._id,
    });

    return token;
  }
}
