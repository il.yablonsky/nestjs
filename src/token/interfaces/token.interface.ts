import { Document, Types } from 'mongoose';

export interface IToken extends Document {
  readonly token: string;
  readonly uId: Types.ObjectId;
  readonly expireAt: string;
}
