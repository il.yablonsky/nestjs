import { Injectable, BadGatewayException } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import { google } from 'googleapis';
import { OAuth2Client } from 'google-auth-library';
import * as R from 'ramda';

import { IMailData } from './interfaces/mail.interface';

@Injectable()
export class MailService {
  oauth2Client: OAuth2Client;

  constructor() {
    this.oauth2Client = new google.auth.OAuth2(
      process.env.MAIL_CLIENT_ID,
      process.env.MAIL_CLIENT_SECRET,
      process.env.MAIL_REDIRECT_URL,
    );

    this.oauth2Client.setCredentials({
      // eslint-disable-next-line @typescript-eslint/camelcase
      refresh_token: process.env.MAIL_REFRESH_TOKEN,
    });
  }

  async send(data: IMailData) {
    const accessToken = await this.oauth2Client.getAccessToken();

    const smtpTransport = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        type: 'OAuth2',
        clientId: process.env.MAIL_CLIENT_ID,
        clientSecret: process.env.MAIL_CLIENT_SECRET,
        user: process.env.MAIL_AUTHOR,
        refreshToken: process.env.MAIL_REFRESH_TOKEN,
        accessToken: accessToken.token,
      },
    });

    try {
      await smtpTransport.sendMail(R.mergeDeepLeft(data, {
        user: process.env.MAIL_AUTHOR,
        refreshToken: process.env.MAIL_REFRESH_TOKEN,
        accessToken,
      }));

      smtpTransport.close();
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }
}
