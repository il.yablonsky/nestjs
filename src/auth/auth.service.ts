import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
  MethodNotAllowedException,
  InternalServerErrorException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt';
import * as R from 'ramda';

import { UserService } from '../user/user.service';
import { TokenService } from '../token/token.service';
import { MailService } from '../mail/mail.service';
import { AvatarService } from '../avatar/avatar.service';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { IUser } from '../user/interfaces/user.interface';

import { SignInDto } from './dto/sign-in.dto';
import { SignOutDto } from './dto/sign-out.dto';
import { ChangePasswordDto } from './dto/change-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { hashPassword } from '../helpers';

@Injectable()
export class AuthService {
  private readonly clientAppUrl: string;

  constructor(
    private readonly userService: UserService,
    private readonly tokenService: TokenService,
    private readonly configService: ConfigService,
    private readonly mailService: MailService,
    private readonly avatarService: AvatarService,
  ) {
    this.clientAppUrl = this.configService.get('FRONT_APP_URL');
  }

  async signUp(createUserDto: CreateUserDto) {
    try {
      const user = await this.userService.create(createUserDto);

      await this.sendConfirmation(user);

      return true;
    } catch (error) {
      throw new BadRequestException('Email is already exist');
    }
  }

  async signIn({ email, password }: SignInDto) {
    const user = await this.userService.findByEmail(email);

    if (user && (await bcrypt.compare(password, user.password))) {
      if (user.status !== 'active') {
        throw new MethodNotAllowedException();
      }

      await this.tokenService.deleteAll(user._id);

      const accessToken = await this.tokenService.getAccessToken(user);
      const avatarItem = await this.avatarService.getAvatar(user._id);

      return {
        _id: user._id,
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        accessToken,
        avatar: avatarItem?.avatar,
      };
    }

    throw new BadRequestException('Invalid credentials');
  }

  async signOut({ token }: SignOutDto) {
    const tokenData = await this.tokenService.verifyToken(token);

    if (!R.isEmpty(tokenData)) {
      await this.tokenService.delete(tokenData._id, token);

      return true;
    }

    throw new UnauthorizedException();
  }

  async changePassword({ token, password }: ChangePasswordDto) {
    try {
      const tokenData = await this.tokenService.findByToken(token);

      if (tokenData) {
        const passwordHash = await hashPassword(password);

        await this.userService.update(tokenData.uId, { password: passwordHash });
        await this.tokenService.deleteAll(tokenData.uId);

        return true;
      }

      throw new BadRequestException('Token is not exist');
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async resetPassword({ email }: ResetPasswordDto) {
    try {
      const user = await this.userService.findByEmail(email);

      if (user) {
        const expiresIn = 60 * 60 * 24; // 1 day
        const expireAt = moment().add(1, 'd').toISOString();
        const tokenPayload = {
          _id: user._id,
          status: user.status,
        };

        const token = await this.tokenService.generateToken(tokenPayload, { expiresIn });
        const confirmLink = `${this.clientAppUrl}/reset-password?token=${token}`;

        await this.tokenService.deleteAll(user._id);
        await this.tokenService.create({ token, uId: user._id, expireAt });
        await this.mailService.send({
          from: this.configService.get('MAIL_AUTHOR'),
          to: user.email,
          subject: 'Reset password',
          html: `
        <div>
          <h3>Hello ${user.firstname}!</h3>
          <p>Please reset your password.</p>
          <p><a href="${confirmLink}">${confirmLink}</a></p>
        </div>
      `,
        });

        return true;
      }

      throw new BadRequestException('Email is not exist');
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async confirmEmail(token: string) {
    const data = await this.tokenService.verifyToken(token);
    const user = await this.userService.find(data._id);

    await this.tokenService.delete(data._id, token);

    if (user && user.status === 'pending') {
      const accessToken = await this.tokenService.getAccessToken(user);

      user.status = 'active';
      user.save();

      return {
        _id: user._id,
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        accessToken,
      };
    }

    throw new BadRequestException();
  }

  async sendConfirmation(user: IUser) {
    const expiresIn = 60 * 60 * 24 * 7; // 1 week
    const expireAt = moment().add(7, 'd').toISOString();
    const tokenPayload = {
      _id: user._id,
      status: user.status,
    };

    const token = await this.tokenService.generateToken(tokenPayload, { expiresIn });
    const confirmLink = `${this.clientAppUrl}/confirm?token=${token}`;

    await this.tokenService.create({ token, uId: user._id, expireAt });
    await this.mailService.send({
      from: this.configService.get('MAIL_AUTHOR'),
      to: user.email,
      subject: 'Verify User',
      html: `
        <div>
          <h3>Hello ${user.firstname}!</h3>
          <p>Please confirm your account.</p>
          <p><a href="${confirmLink}">${confirmLink}</a></p>
        </div>
      `,
      text: '<b>welcome</b>',
    });
  }
}
