import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignOutDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  token: string
}
