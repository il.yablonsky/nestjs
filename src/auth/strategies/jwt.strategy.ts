import { Injectable, UnauthorizedException, Req } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';

import { IUser } from '../../user/interfaces/user.interface';
import { TokenService } from '../../token/token.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configService: ConfigService,
    private readonly tokenService: TokenService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET'),
      passReqToCallback: true,
    });
  }

  async validate(@Req() req: Request, user: Partial<IUser>) {
    const token = req.headers.authorization.slice(7);
    const isTokenExist = await this.tokenService.exists(user._id, token);

    if (isTokenExist) {
      return user;
    }

    throw new UnauthorizedException();
  }
}
