import { Module } from '@nestjs/common';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TokenModule } from '../token/token.module';
import { UserModule } from '../user/user.module';
import { MailModule } from '../mail/mail.module';
import { AvatarModule } from '../avatar/avatar.module';
import { configModule } from '../config.root';

@Module({
  imports: [
    UserModule,
    TokenModule,
    MailModule,
    AvatarModule,
    configModule,
  ],
  providers: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
