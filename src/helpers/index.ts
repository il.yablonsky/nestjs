import * as R from 'ramda';
import * as bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;

export const isNotNil = R.complement(R.isNil);
export const isNotEmpty = R.complement(R.isEmpty);
export const isEmptyOrNil = R.anyPass([R.isNil, R.isEmpty]);
export const zeroOrNil = R.anyPass([R.isNil, R.compose(R.equals(0), Number)]);
export const isFunction = R.is(Function);
export const isBoolean = R.is(Boolean);

/**
 * Converts passed value to `String` type (if needed)
 */
export const toString = R.cond([
  [R.isNil, R.always('')],
  [R.is(String), R.identity],
  [R.T, R.toString],
]);

export const hashPassword = async (password: string) => {
  const salt = await bcrypt.genSalt(SALT_ROUNDS);

  return await bcrypt.hash(password, salt);
};
